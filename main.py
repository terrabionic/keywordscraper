import concurrent.futures
import pandas as pd
import itertools
import requests
import string
import json
import time

startTime = time.time()

WAIT_TIME = 0.1
MAX_WORKERS = 20

# lenguaje de keyword
lang = "es"

charList = " " + string.ascii_lowercase + string.digits


def makeGoogleRequest(query):
    #
    time.sleep(WAIT_TIME)
    URL = "http://suggestqueries.google.com/complete/search"
    PARAMS = {"client": "firefox",
              "hl": lang,
              "q": query}
    headers = {'User-agent': 'Mozilla/5.0'}
    response = requests.get(URL, params=PARAMS, headers=headers)
    if response.status_code == 200:
        suggestedSearches = json.loads(response.content.decode('utf-8'))[1]
        return suggestedSearches
    else:
        return "ERR"


def getGoogleSuggests(keyword):
    # err_count1 = 0
    queryList = [keyword + " " + char for char in charList]
    suggestions = []
    for query in queryList:
        suggestion = makeGoogleRequest(query)
        if suggestion != 'ERR':
            suggestions.append(suggestion)

    # Remove empty suggestions
    suggestions = set(itertools.chain(*suggestions))
    if "" in suggestions:
        suggestions.remove("")
    return suggestions


# valores de prueba
keywords = ['ganado']

resultList = []

with concurrent.futures.ThreadPoolExecutor(max_workers=MAX_WORKERS) as executor:
    futuresGoogle = {executor.submit(getGoogleSuggests, keyword): keyword for keyword in keywords}

    for future in concurrent.futures.as_completed(futuresGoogle):
        key = futuresGoogle[future]
        for suggestion in future.result():
            resultList.append([key, suggestion])

# resultado a dataframe
outputDf = pd.DataFrame(resultList, columns=['Keyword', 'Suggestion'])

outputDf.to_csv('keyword_sugg_res.csv', index=True)
print('keyword_sugg_res.csv File Saved')

print(f"Execution time: {(time.time() - startTime) :.2f} sec")
